## Project Description

The Company has undertaken an important project which specifies and demands the
company being aligned with industry standards for security. Currently, The Company uses a
third party software to meet that needs, but as the company grows, more requirement arise.
The company needs to have a single point of truth in order to generate a different kind of
reports and to support the needs of the accountant office, as for example, to calculate the
overtimes for each of the employees. As a result, the company decided to create an
application supporting both, the access control system of the building and the reports for the
accountant office through a web-based application that will be also developed by the
company.

The Company Innovation Center, is hosted on a three-level building
and each of the levels, identifies a specific area. Different type of employees has different
access level per area. For each of the employees, we need to keep in the database basic
information for their profile like, first and last name and a unique id. Every employee belongs
to an employee type and each of types is recognized with a unique id and a description. Each
type of the employee is capable of accessing many different areas inside the building.
Company’s security standards demand to keep a very detailed access history for each of the
employees and the related areas, keeping information about the date and time of the access
as well as the action type of the employee (IN or OUT).

### PART 1 – Database design and implementation

As the developer of the system, you must identify the basic entities along with their relationships, in order to support the above-described requirements. 
You have to design the Entity Relationship diagram of your solution before you proceed the implementation of the actual database in your MySql database.

For business continuation purposes you must make use of the provided XML, in order to insert data from the previous system into your own database. Before that, ensure the provided xml is valid.

In order to do that you must create a simple JAVA console application that will be able to parse and insert information residing on XML file into the new system.

### PART 2 – Web Services

The Company has undertaken to design and implement a RESTful API for a client, which should provide functionality for the following use cases (The responses must be in JSON format):

1. Provide the list of the employees (optionally: provide functionality for filtering by name or id – provide functionality for pagination )
2. Provide the list of all the company’s employees along with their access history (optionally: provide functionality for filtering by name or id – provide functionality for pagination )
3. Provide the list of the employees, with more than 3 hours overtime with in a given date time window (optionally: provide functionality for filtering by name or id – provide functionality for pagination )

4. Given an employee id, provide the list of their access history (optionally: provide functionality for filtering by name or id – provide functionality for pagination )
5. Given an employee id and a time window provide the list of their access history for that specific time window (optionally: provide functionality for filtering by name or id – provide functionality for pagination )
6. Given an employee id provide us the number of actions (IN-OUT) per day for a given time window
7. Given a time window provide us information about the earliest IN and the latest OUT action along with the corresponding employee information

- Optionally, Create RESTful services to support the CRUD operations for an employee
- Optionally, Create RESTful services to support the CRUD operations for the access history of a specific employee
- Optionally, Register a new user
- Optionally, Login an existing user
- Optionally, Get employee’s profile information based on Id

In case you implement the registration and the login REST services, you have to secure your services accordingly, using the JWT approach.

Please provide us your solution to the above problem along with:

- The database ER Diagram
- The database implementation
- The JAVA implementation
- Test cases for each of the supported scenarios to demonstrate the requested functionality


### [Wiki](https://bitbucket.org/giannosfor/access-control-system/wiki/Home)
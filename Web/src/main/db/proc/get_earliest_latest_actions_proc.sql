create
    definer = giannis@`%` procedure Get_Earliest_Latest_Action(IN theDate date)
BEGIN
select * from
    (select ACTION, TIMESTAMP, employee_id  from access_action
    where DATE(TIMESTAMP) = theDate
      AND ACTION = 'IN'
    order by TIMESTAMP asc
    lIMIT 1) a
    UNION
    (select ACTION, TIMESTAMP, employee_id  from access_action
    where DATE(TIMESTAMP) = theDate
      AND ACTION = 'OUT'
    order by TIMESTAMP desc
    LIMIT 1);
END;

# call Get_Earliest_Latest_Action('2017-05-30');
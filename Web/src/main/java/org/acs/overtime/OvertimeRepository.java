package org.acs.overtime;

import org.acs.dto.EmployeeDto;
import org.acs.dto.OvertimeDto;
import org.acs.employee.EmployeeRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class OvertimeRepository {

    @PersistenceContext EntityManager entityManager;

    final EmployeeRepository employeeRepository;

    public OvertimeRepository(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<OvertimeDto> getOvertimes(LocalDate fromDate, LocalDate toDate) {

        Date sqlFromDate = Date.valueOf(fromDate);
        Date sqlToDate = Date.valueOf(toDate);

        StoredProcedureQuery procedureQuery = entityManager
                .createStoredProcedureQuery("Get_Working_Hours")

        .registerStoredProcedureParameter("fromDate", Date.class, ParameterMode.IN)
        .registerStoredProcedureParameter("toDate", Date.class, ParameterMode.IN)

        .setParameter("fromDate", sqlFromDate)
        .setParameter("toDate", sqlToDate);

        procedureQuery.execute();

        List<Object[]> rsList = procedureQuery.getResultList();

        return rsList.stream()
                .map(  rs -> {
                    Time overtimes = ( (Time) rs[0] );
                    EmployeeDto employee = employeeRepository.findOne( ( (BigInteger) rs[1] ).longValue() , false);
                    return new OvertimeDto(overtimes, employee);
                }).collect(Collectors.toList());
    }
}

package org.acs.overtime;

import org.acs.dto.OvertimeDto;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@RestController
public class OvertimeController {

    final OvertimeRepository overtimeRepository;

    public OvertimeController(OvertimeRepository overtimeRepository) {
        this.overtimeRepository = overtimeRepository;
    }

    @GetMapping("/overtimes/{from}/fromTo/{to}")
    List<OvertimeDto> overtimes(
            @PathVariable("from") @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate fromDate,
            @PathVariable("to") @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate toDate
            ) {

        return overtimeRepository.getOvertimes(fromDate, toDate);
    }
}
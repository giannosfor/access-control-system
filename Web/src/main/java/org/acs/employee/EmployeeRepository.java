package org.acs.employee;

import org.acs.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface EmployeeRepository extends JpaRepository<Employee, Long>, EmployeeRepositoryCustom {
    Employee findByFirstName(String firstname);
    Employee findByLastName(String lastname);
    Employee findByFirstNameAndLastName(String firstname, String lastname);
    Employee findById(Long id);
    void deleteById(Long id);
}
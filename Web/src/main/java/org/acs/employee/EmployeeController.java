package org.acs.employee;

// JWT

import org.acs.dto.EmployeeDto;
import org.acs.mapper.AcsMapper;
import org.acs.model.Employee;
import org.acs.model.EmployeeType;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Optional;

@RestController
class EmployeeController {

    final EmployeeRepository employeeRepository;
    final EmployeeTypeRepository employeeTypeRepository;

    private final AcsMapper acsMapper = Mappers.getMapper(AcsMapper.class);

    public EmployeeController(EmployeeRepository employeeRepository, EmployeeTypeRepository employeeTypeRepository) {
        this.employeeRepository = employeeRepository;
        this.employeeTypeRepository = employeeTypeRepository;
    }

    @GetMapping("/employees")
    Page<EmployeeDto> all(
            @RequestParam(value="accessactions", required = false) boolean accessHistory,
            Pageable pageable
    ) {
        return employeeRepository.allWithAccessHistory(accessHistory, pageable);
    }

    @GetMapping("/employee")
    EmployeeDto oneFilter(
            @RequestParam(value="firstname", required = false) String firstname,
            @RequestParam(value="lastname", required = false) String lastname,
            @RequestParam(value="accessactions", required = false) boolean accessHistory
    ) {
        Employee employee = null;
        if (null != firstname && null != lastname) employee = employeeRepository.findByFirstNameAndLastName(firstname, lastname);
        else if (null != firstname) employee = employeeRepository.findByFirstName(firstname);
        else if (null != lastname)   employee = employeeRepository.findByLastName(lastname);

        if (null != employee && ! accessHistory) employee.setAccessActions(Collections.emptyList());
        return acsMapper.employeeToEmployeeDTO(employee);
    }

    @GetMapping("/employees/{id}")
    EmployeeDto one(@PathVariable Long id, @RequestParam(required = false) boolean accessHistory) {
        return employeeRepository.findOne(id, accessHistory);
    }

    @PostMapping("/employees")
    Employee newEmployee(@RequestBody Employee newEmployee) {

        String description = newEmployee.getEmployeeType().getDescription();
        EmployeeType employeeType = employeeTypeRepository.findByDescription(description);

        newEmployee.setEmployeeType(employeeType);
        return employeeRepository.save(newEmployee);
    }

    @PutMapping("/employees/{id}")
    Employee updateEmployee(@RequestBody Employee newEmployee, @PathVariable Long id) {

        return Optional.of( employeeRepository.findById(id) ).map(employee -> {
            employee.setFirstName(newEmployee.getFirstName());
            employee.setLastName(newEmployee.getLastName());

            EmployeeType employeeType = employeeTypeRepository.findByDescription(newEmployee.getEmployeeType().getDescription());
            employee.setEmployeeType(employeeType);

            return employeeRepository.save(employee);
        }).orElseGet(() -> {
            return employeeRepository.save(newEmployee);
        });
    }

    @DeleteMapping("/employees/{id}")
    void deleteEmployee(@PathVariable Long id) {
        employeeRepository.deleteById(id);
    }
}
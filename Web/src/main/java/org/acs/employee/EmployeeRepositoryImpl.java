package org.acs.employee;

import org.acs.dto.EmployeeDto;
import org.acs.mapper.AcsMapper;
import org.acs.model.Employee;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public class EmployeeRepositoryImpl implements EmployeeRepositoryCustom {

    @Autowired @Lazy EmployeeRepository employeeRepository;

    private final AcsMapper acsMapper = Mappers.getMapper(AcsMapper.class);

    @Override public Page<EmployeeDto> allWithAccessHistory(boolean flag, Pageable pageable) {
        Page<Employee> pageEmployees = employeeRepository.findAll(pageable);

        if (flag) pageEmployees.getContent().stream().forEach(employee -> employee.getAccessActions()); // 1804116 // 16835
        // TODO: If access action is not requested map to dto without access action list
        else pageEmployees.getContent().stream().forEach(employee -> employee.setAccessActions(Collections.EMPTY_LIST));

        List<EmployeeDto> employeeDtos =  acsMapper
                .listOfEmployeeToListOfEmployeeDTO(pageEmployees.getContent());

        PageImpl pageImpl = new PageImpl(employeeDtos, pageable, pageEmployees.getTotalElements());

        return pageImpl;
    }

    @Override public EmployeeDto findOne(Long id, boolean accessHistory) {
        Employee employee = employeeRepository.findOne(id);
        if (! accessHistory) employee.setAccessActions(Collections.emptyList());
        return acsMapper.employeeToEmployeeDTO(employee);
    }
}
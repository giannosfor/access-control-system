package org.acs.employee;

import org.acs.dto.EmployeeDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EmployeeRepositoryCustom {
    Page<EmployeeDto> allWithAccessHistory(boolean flag, Pageable pageable);
    EmployeeDto findOne(Long id, boolean accessHistory);

}

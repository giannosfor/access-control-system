package org.acs.user;

import org.acs.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface UserRepository extends JpaRepository<User, Long> {
    User findByFirstname(String firstname);
    User findByFirstnameAndLastname(String firstname, String lastname);
}

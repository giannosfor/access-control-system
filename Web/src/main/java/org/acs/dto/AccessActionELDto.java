package org.acs.dto;

import lombok.Builder;
import lombok.Data;
import org.acs.model.Action;

@Data
@Builder
public class AccessActionELDto {
    Action action;
    String timestamp;
    EmployeeDto employeeDto;
}

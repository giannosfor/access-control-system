package org.acs.dto;

import lombok.*;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class EmployeeDto {
    private Long id;
    private String firstName;
    private String lastName;
    private EmployeeTypeDto employeeType;
    private Set<AccessActionDto> accessActions;
}

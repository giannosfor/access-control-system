package org.acs.dto;

import lombok.*;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class LevelDto {
    private Long id;
    private String description;
}
package org.acs.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.acs.model.Action;

import java.util.Date;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class AccessActionCustomDto {
    @JsonIgnore
    private Long id;
    @JsonProperty("level")
    private LevelDto levelDto;
    private Action action;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date timestamp;
    @JsonProperty("employee")
    private EmployeeCustomDto employeeDto;
}

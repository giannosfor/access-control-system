package org.acs.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class EmployeeCustomDto {
    private Long id;
    private String firstName;
    private String lastName;
    private EmployeeTypeDto employeeType;
}


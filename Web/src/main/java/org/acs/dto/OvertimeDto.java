package org.acs.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Time;

@Data
@AllArgsConstructor
public class OvertimeDto {
    private Time time;
    private EmployeeDto employee;
}

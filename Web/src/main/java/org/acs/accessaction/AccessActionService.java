package org.acs.accessaction;

import org.acs.dto.AccessActionELDto;
import org.acs.dto.EmployeeDto;
import org.acs.employee.EmployeeRepository;
import org.acs.model.Action;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class AccessActionService {

    final AccessActionRepository accessActionRepository;
    final EmployeeRepository employeeRepository;

    public AccessActionService(AccessActionRepository accessActionRepository, EmployeeRepository employeeRepository) {
        this.accessActionRepository = accessActionRepository;
        this.employeeRepository = employeeRepository;
    }

    public List<AccessActionELDto> getEarliestLatestAction(LocalDate fromDate, LocalDate toDate) {
        List<Object[]> actionL = new ArrayList<>();
        for (LocalDate date = fromDate; date.isBefore(toDate); date = date.plusDays(1))
            actionL.addAll( accessActionRepository.getEarliestLatestActionForDate(date) );

        return convertIActionToDto(actionL);
    }


    private List<AccessActionELDto> convertIActionToDto(List<Object[]> actions) {

        List<AccessActionELDto> accessActionELDtoList = new ArrayList<>();
        for ( Object[] ac : actions) {

            Action action = Action.valueOf((String) ac[0]);

            Timestamp sqlTimestamp = (Timestamp) ac[1];
            long employeeId = ((BigInteger) ac[2] ).longValue();

            String timestamp = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(sqlTimestamp);

            EmployeeDto employeeDto = employeeRepository.findOne( employeeId, false );

            AccessActionELDto accessActionELDto = AccessActionELDto.builder()
                    .employeeDto(employeeDto)
                    .timestamp(timestamp)
                    .action(action).build();

            accessActionELDtoList.add(accessActionELDto);
        }

        return accessActionELDtoList;
    }
}
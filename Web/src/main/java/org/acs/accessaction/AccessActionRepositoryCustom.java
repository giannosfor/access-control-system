package org.acs.accessaction;

import org.acs.model.AccessAction;

import java.time.LocalDate;
import java.util.List;

public interface AccessActionRepositoryCustom {

    List<Object[]> getEarliestLatestActionForDate(LocalDate theDate);
}

package org.acs.accessaction;

import org.acs.dto.AccessActionCustomDto;
import org.acs.dto.AccessActionDto;
import org.acs.dto.AccessActionELDto;
import org.acs.employee.EmployeeRepository;
import org.acs.mapper.AcsMapper;
import org.acs.model.AccessAction;
import org.acs.model.Employee;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
public class AccessActionController {
    final AcsMapper acsMapper = Mappers.getMapper(AcsMapper.class);

    final AccessActionRepository accessActionRepository;
    final EmployeeRepository employeeRepository;
    final AccessActionService accessActionService;

    public AccessActionController(AccessActionRepository accessActionRepository, EmployeeRepository employeeRepository, AccessActionService accessActionService) {
        this.accessActionRepository = accessActionRepository;
        this.employeeRepository = employeeRepository;
        this.accessActionService = accessActionService;
    }

    @GetMapping("/accessactions/employee/{id}")
    Page<AccessActionDto> findByEmployee(
            @PathVariable Long id,
            Pageable pageable
    ) { // TODO: Check if the query use a limit in the result or just returns a span of elements
        Page<AccessAction> accessActions = accessActionRepository.findByEmployeeId(id, pageable);
        List<AccessActionDto> accessActionDtos = acsMapper.listOfAccessActionToListOfAccessActionDTO(accessActions.getContent());

        PageImpl pageImpl = new PageImpl(accessActionDtos, pageable, accessActions.getTotalElements());

        return pageImpl;
    }

    @GetMapping("/accessactions/{from}/fromTo/{to}/employee/{id}")
    Page<AccessActionDto> overtimes(
            @PathVariable("from") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fromDate,
            @PathVariable("to") @DateTimeFormat(pattern = "yyyy-MM-dd") Date toDate,
            @PathVariable("id") Long id,
            Pageable pageable
    ) {
        Employee employee = employeeRepository.findOne(id);
        Page<AccessAction> accessActions = accessActionRepository.queryAccessActionByEmployeeAndPeriod(employee, fromDate, toDate, pageable);
        List<AccessActionDto> accessActionDtos = acsMapper.listOfAccessActionToListOfAccessActionDTO(accessActions.getContent());

        PageImpl pageImpl = new PageImpl(accessActionDtos, pageable, accessActions.getTotalElements());

        return pageImpl;
    }

    @GetMapping("/accessactions/count/{from}/fromTo/{to}/employee/{id}")
    List<ICountActions> count(
            @PathVariable("from") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fromDate,
            @PathVariable("to") @DateTimeFormat(pattern = "yyyy-MM-dd") Date toDate,
            @PathVariable("id") Long id
    ) {
        List<ICountActions> accessActions = accessActionRepository.countAccessActionByEmployeeAndPeriod(id, fromDate, toDate);
        return accessActions;
    }

    @GetMapping("/accessactions/earliestlatest/{from}/fromTo/{to}")
    List<AccessActionELDto> getEarliestLatest(
            @PathVariable("from") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fromDate,
            @PathVariable("to") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate toDate
    ) {
        return accessActionService.getEarliestLatestAction(fromDate, toDate);
    }

    @GetMapping("/accessactions/{id}")
    AccessActionCustomDto findOne(
            @PathVariable Long id
    ) {
        AccessAction accessAction = accessActionRepository.findByIdFetchEmployee(id);
        return acsMapper.accessActionToAccessActionCustomDTO(accessAction);
    }

    @PostMapping("/accessactions")
    AccessAction newAccessAction(@RequestBody AccessAction accessAction) {
        return accessActionRepository.save(accessAction);
    }

    @PutMapping("/accessactions/{id}")
    AccessAction updateAccessAction(@RequestBody AccessAction newAccessAction, @PathVariable Long id) {

        return Optional.of( accessActionRepository.findById(id) ).map(accessAction -> {
            accessAction.setAction(newAccessAction.getAction());
            accessAction.setEmployee(newAccessAction.getEmployee());
            accessAction.setLevel(newAccessAction.getLevel());
            return accessActionRepository.save(newAccessAction);
        }).orElseGet(() -> {
            return accessActionRepository.save(newAccessAction);
        });
    }

    @DeleteMapping("/accessactions/{id}")
    void deleteAccessAction(@PathVariable Long id) {
        accessActionRepository.deleteById(id);
    }
}

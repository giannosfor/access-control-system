package org.acs.accessaction;

import org.acs.model.AccessAction;
import org.acs.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Transactional
interface AccessActionRepository extends JpaRepository<AccessAction, Long>, AccessActionRepositoryCustom {

    AccessAction findById(Long id);
    void deleteById(Long id);

    @Query("SELECT ac FROM AccessAction ac " +
            "JOIN FETCH ac.employee " +
            "WHERE ac.id = :id ")
    AccessAction findByIdFetchEmployee(
            @Param("id") Long id);

    @Query("SELECT ac FROM AccessAction ac " +
                "WHERE ac.timestamp >= :fromDate " +
                    "AND ac.timestamp <= :toDate " +
                    "AND ac.employee = :employee")
    Page<AccessAction> queryAccessActionByEmployeeAndPeriod(
            @Param("employee") Employee employee,
            @Param("fromDate") Date fromDate,
            @Param("toDate") Date toDate,
            Pageable pageable);

    @Query(value = "SELECT count(*) as count, DATE(ac.timestamp) as date " +
            "FROM access_action ac " +
            "WHERE ac.timestamp >= :fromDate " +
                "AND ac.timestamp <= :toDate " +
                "AND ac.employee_id = :employeeID " +
            "GROUP BY date", nativeQuery = true)
     List<ICountActions> countAccessActionByEmployeeAndPeriod(
            @Param("employeeID") Long employeeID,
            @Param("fromDate") Date fromDate,
            @Param("toDate") Date toDate);

    @Query("SELECT ac FROM AccessAction ac " +
            "WHERE ac.employee.id = :id ")
    Page<AccessAction> findByEmployeeId(
            @Param("id") Long id, Pageable pageable);
}
package org.acs.accessaction;

import org.acs.model.AccessAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Repository
@Transactional
public class AccessActionRepositoryImpl implements AccessActionRepositoryCustom {

    @PersistenceContext
    EntityManager entityManager;

    @Autowired @Lazy
    AccessActionRepository accessActionRepository;

    @Override
    public List<Object[]> getEarliestLatestActionForDate(LocalDate theDate) {

            Date sqlDate = Date.valueOf(theDate);

            StoredProcedureQuery procedureQuery = entityManager
                    .createStoredProcedureQuery("Get_Earliest_Latest_Action")
                    .registerStoredProcedureParameter("theDate", Date.class, ParameterMode.IN)
                    .setParameter("theDate", sqlDate);

            procedureQuery.execute();

            return procedureQuery.getResultList();
        }

}

package org.acs.accessaction;

import java.sql.Date;
// Fail
public interface ICountActions {
    int getCount();
    Date getDate();
}
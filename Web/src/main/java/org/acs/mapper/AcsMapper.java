package org.acs.mapper;

import org.acs.dto.*;
import org.acs.model.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper
public interface AcsMapper {
    List<EmployeeDto> listOfEmployeeToListOfEmployeeDTO(List<Employee> employees);
    EmployeeDto employeeToEmployeeDTO(Employee entity);
    EmployeeTypeDto employeeTypeToEmployeeTypeDTO(EmployeeType employeeType);
    EmployeeCustomDto employeeToEmployeeCustomDTO(Employee employee);
    @Mappings({
            @Mapping(source = "level", target = "levelDto"),
    })
    AccessActionDto accessActionToAccessActionDTO(AccessAction accessAction);
    @Mappings({
            @Mapping(source = "levelDto", target = "level"),
    })
    AccessAction accessActionDtoToAccessAction(AccessActionDto accessActionDto);
    @Mappings({
            @Mapping(source = "level", target = "levelDto"),
            @Mapping(source = "employee", target = "employeeDto"),
    })
    AccessActionCustomDto accessActionToAccessActionCustomDTO(AccessAction accessAction);
    List<AccessActionDto> listOfAccessActionToListOfAccessActionDTO(List<AccessAction> accessActions);

    LevelDto levelToLevelDTO(Level level);
}
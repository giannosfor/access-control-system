package org.acs.model;

public enum Action {
    IN("in"),
    OUT("out");

    private String action;
    Action(String a) {
        action = a;
    }
}

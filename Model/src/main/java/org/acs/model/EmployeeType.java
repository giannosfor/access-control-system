package org.acs.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor
@Table(name = "employee_type")
public class EmployeeType {
    @Id
    private Long id;
    private String description;
}


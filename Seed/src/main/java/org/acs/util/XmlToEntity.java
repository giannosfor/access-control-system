package org.acs.util;

import lombok.Getter;
import org.acs.model.*;
import org.acs.model.Employee;
import org.acs.model.EmployeeType;
import org.acs.model.User;
import org.acs.xml.*;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

class XmlToEntity {

    @Getter private List<Employee> employees;
    @Getter private List<EmployeeType> employeeTypes;
    @Getter private List<Level> levels;
    @Getter private List<User> users;

    public static XmlToEntity init(AccessCatalog accessCatalog) {
        XmlToEntity xmlToEntity = new XmlToEntity(accessCatalog);

        xmlToEntity.employeeTypes = xmlToEntity.transform(accessCatalog.getEmployeeTypes());
        xmlToEntity.levels = xmlToEntity.transform(accessCatalog.getAreas());
        xmlToEntity.employees = xmlToEntity.transform(accessCatalog.getEmployees());
        xmlToEntity.users = xmlToEntity.transform(accessCatalog.getUsers());
        return xmlToEntity;
    }

    private List<User> transform(Users users) {
        return users.getUser().stream()
                .map(user -> {
                    return new User(
                            Long.valueOf(user.getId()),
                            user.getFirstName(),
                            user.getLastName(),
                            user.getPassword()
                    );
                }).collect(Collectors.toList());
    }

    private XmlToEntity(AccessCatalog accessCatalog) {
        transform(accessCatalog.getEmployeeTypes());
    }

    private List<AccessAction> transform(AccessHistory accessHistory) {

        Map<Long, Level> levelMap = levels.stream().collect(
                Collectors.toMap(Level::getId, level -> level));

        return accessHistory.getAccess().stream()
                .map(access -> {
                    return new AccessAction(
                            levelMap.get(Long.valueOf(access.getAreaId())),
                            access.getΤype().equals("in") ? Action.IN : Action.OUT,
                            access.getDateΤime().toGregorianCalendar().getTime()
                    );
                }).collect(Collectors.toList());
    }

    private static List<Level> transform(Areas areas) {
        return areas.getArea().stream()
                .map(area -> {
                    return new Level(
                            Long.valueOf(area.getId()),
                            area.getDescription()
                    );
                }).collect(Collectors.toList());
    }

    public List<EmployeeType> transform(EmployeeTypes employeeTypes) {
        return employeeTypes.getEmployeeType()
                .stream()
                .map(empTp ->
                        new EmployeeType(
                                Long.valueOf(empTp.getId()),
                                empTp.getDescription()
                        )).collect(Collectors.toList());
    }

    public  List<Employee> transform(Employees employees) {
        Function<Long, EmployeeType> findEmployeeTypeById =
                emplTypeId -> this.employeeTypes.stream()
                        .filter( emplT -> Long.valueOf(emplTypeId).equals(emplT.getId()) )
                        .findFirst().get();

        return employees
                .getEmployee()
                .stream()
                .map(emp -> {
                    return new Employee(
//                            Long.valueOf( emp.getId() ),
                            null,
                            emp.getFirstName(),
                            emp.getLastName(),
                            findEmployeeTypeById.apply(Long.valueOf(emp.getEmployeeTypeId())),
                            transform(emp.getAccessHistory())
                    );
                }).collect(Collectors.toList());
    }
}

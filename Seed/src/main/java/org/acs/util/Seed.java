package org.acs.util;

import org.acs.xml.AccessCatalog;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Seed {

    private static final String PERSISTENCE_UNIT_NAME = "acs";

    public static void main(String[] args) {

        try (InputStream inStream = new FileInputStream( args[0] );
        ) {
            AccessCatalog accessCatalog = unmarshalXml(AccessCatalog.class,inStream);
            XmlToEntity xmlToEntity = XmlToEntity.init(accessCatalog);

            EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
            EntityManager em = factory.createEntityManager();

            em.getTransaction().begin();
            xmlToEntity.getLevels().forEach(level -> em.persist(level));
            em.getTransaction().commit();

            em.getTransaction().begin();
            xmlToEntity.getEmployeeTypes().forEach(employeeType -> em.persist(employeeType));
            em.getTransaction().commit();

            em.getTransaction().begin();
            xmlToEntity.getEmployees().forEach(empl -> em.persist(empl) );
            xmlToEntity.getUsers().forEach(user -> em.persist(user) );
            em.getTransaction().commit();

            em.close();

        } catch (JAXBException | IOException e) {
            e.printStackTrace();
        }

    }

    private static <T> T unmarshalXml(Class<T> tClass, InputStream inStream) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance( AccessCatalog.class );
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return (T) jaxbUnmarshaller.unmarshal( inStream );
    }
}
